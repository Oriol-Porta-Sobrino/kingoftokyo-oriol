﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyoOriol
{
    public class Partida
    {
        public Partida()
        {
        }

        public Partida(int turno, int jugadores)
        {
            Turno = turno;
            Jugadores = jugadores;
        }

        public int PartidaID { get; set; }
        public int Turno { get; set; }
        public int Jugadores { get; set; }
        public virtual ICollection<Monstruo> Monstruos { get; set; }


        //Atributos anadidos
        public int Tokio { get; set; }

    }
}
