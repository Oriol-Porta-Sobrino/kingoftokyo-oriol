﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KingOfTokyoOriol
{

    public class Monstruo
    {

        public Monstruo()
        {
        }

        public Monstruo(string nombre, int costeEnergia)
        {
            Nombre = nombre;
            CosteEnergia = costeEnergia;
        }

        public Monstruo(string nombre, int vidas, int puntos, int energia, bool tokio, bool vivo)
        {
            Nombre = nombre;
            Vidas = vidas;
            Puntos = puntos;
            Energia = energia;
            Tokio = tokio;
            Vivo = vivo;
        }

        public int MonstruoID { get; set; }   
        public string Nombre { get; set; }
        public int Vidas { get; set; }
        public int Puntos { get; set; }
        public int Energia { get; set; }
        public Boolean Tokio { get; set; }
        public Boolean Vivo { get; set; }
        public int CosteEnergia { get; set; }
        public Boolean Poseido { get; set; }
        public virtual Monstruo Poder { get; set; }
        public virtual Jugador Jugador { get; set; }
        

    }
}
