﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KingOfTokyoOriol
{
    class Program
    {
        private static readonly int PLAYER = 4;
        
        static void Main(string[] args)
        {
            using (var sesion = new Sesion())
            {
                // Creación de jugadores disponibles         
                List<Jugador> jugadores = new List<Jugador>();      
                jugadores.Add(new Jugador("Homer", "Simpson"));
                jugadores.Add(new Jugador("Rocco", "Siffredi"));
                jugadores.Add(new Jugador("Barack", "Obama"));
                jugadores.Add(new Jugador("Michael", "Jackson"));
                jugadores.Add(new Jugador("Lionel", "Messi"));
                jugadores.Add(new Jugador("Osama", "Bin Laden"));
                jugadores.Add(new Jugador("Bruce", "Willis"));
                jugadores.Add(new Jugador("Bill", "Gates"));
               

                // Creación de monstruos disponibles
                List<Monstruo> monstruos = new List<Monstruo>();        
                monstruos.Add(new Monstruo("King", 10, 0, 0, false, true));
                monstruos.Add(new Monstruo("MekaDracorn", 10, 0, 0, false, true));
                monstruos.Add(new Monstruo("Ciberkitty", 10, 0, 0, false, true));
                monstruos.Add(new Monstruo("Gigazaur", 10, 0, 0, false, true));
                monstruos.Add(new Monstruo("Alienoid", 10, 0, 0, false, true));
                monstruos.Add(new Monstruo("King", 10, 0, 0, false, true));

                Boolean ganador = false;
                Monstruo monstruo = Start(ref monstruos, ref jugadores, sesion);
            
                //Tras haber guardado en bd los monstruos que jugarán la partida añadimos los monstruos de poder.
                sesion.Monstruos.Add(new Monstruo("Flamigero", 3));
                sesion.Monstruos.Add(new Monstruo("Mimetismo", 8));
                sesion.Monstruos.Add(new Monstruo("Rayo", 6));
                sesion.Monstruos.Add(new Monstruo("Veneno", 4));
                sesion.SaveChanges();

                while (!ganador)
                {
                    int[] dados = Roll();
                    SolveRoll(dados, sesion, ref monstruo);
                    SolvePowerCarts(sesion, ref monstruo);
                    monstruo = Reassign(sesion, ref monstruo, ref ganador);
                    if (ganador)
                    {
                        Console.WriteLine("Felicidades " + monstruo.Jugador.Nombre + " " + monstruo.Jugador.Apellido + "!!! has ganado la partida!");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("Turno del jugador " + monstruo.Jugador.JugadorID + " (" + monstruo.Jugador.Nombre + " " + monstruo.Jugador.Apellido + ")");
                    }
                }
            }
        }

        //Metodos Auxiliares

        public static Boolean ComprobarVivo(Sesion sesion, ref Monstruo monstruo)
        {
            Boolean entra = false;
            List<Monstruo> lista = sesion.Monstruos.ToList();
            foreach (Monstruo mon in lista)
            {
                if (mon.Vivo && mon.Vidas <= 0)
                {
                    if (mon.Tokio)
                    {
                        mon.Tokio = false;
                        monstruo.Tokio = true;
                        entra = true;
                        Console.WriteLine("El monstruo " + mon.Nombre + " ha abandonado Tokio");
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha entrado a Tokio");
                        sesion.SaveChanges();
                    }
                    mon.Vivo = false;
                    Console.WriteLine("El monstruo " + mon.Nombre + " ha muerto, el jugador " + mon.Jugador.Nombre + " ha perdido" );
                    sesion.SaveChanges();
                }
            }
            return entra;
            
        }

        public static void EntrarTokio(ref Monstruo fuera, ref Monstruo dentro, Sesion sesion)
        {
            Random random = new Random();
            int rnd = random.Next(2);
            if (rnd == 0)
            {
                dentro.Tokio = false;
                fuera.Tokio = true;
                sesion.SaveChanges();
                Console.WriteLine("El monstruo " + dentro.Nombre + " ha salido de Tokio");
                Console.WriteLine("El monstruo " + fuera.Nombre + " ha entrado a Tokio");
            }
            else
            {
                Console.WriteLine("El monstruo " + fuera.Nombre + " ha decidido no entrar a Tokio");
            }
        }

        public static void UsarPoder(ref Monstruo monstruo, Sesion sesion, int pod)
        {
            Random random = new Random();
            List<Monstruo> lista = sesion.Monstruos.ToList();
            int rnd;
            switch (pod)
            {
                case 3:
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha usado el poder Aliento Flamigero");
                    foreach (Monstruo mon in lista)
                    {
                        if (!mon.Equals(monstruo) && mon.Vivo)
                        {
                            mon.Vidas = mon.Vidas - 1;
                            if (mon.Tokio)
                            {
                                Monstruo nece = mon;
                                EntrarTokio(ref monstruo, ref nece, sesion);
                            }
                            sesion.SaveChanges();                           
                            Console.WriteLine("El monstruo " + mon.Nombre + " ha perdido 1 vida, ahora tiene " + mon.Vidas);
                        }
                    }
                    ComprobarVivo(sesion, ref monstruo);
                    monstruo.Poder.Poseido = false;
                    monstruo.Poder = null;
                    sesion.SaveChanges();
                    break;
                case 4:
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha usado el poder Monstruo escupidor de veneno");
                    List<Monstruo> mons = new List<Monstruo>();
                    foreach (Monstruo mon in lista)
                    {
                        if (!mon.Equals(monstruo) && mon.Vivo && mon.Puntos > 0)
                        {
                            mons.Add(mon);
                        }
                    }
                    if (mons.Count != 0)
                    {
                        rnd = random.Next(mons.Count());
                        mons[rnd].Puntos = mons[rnd].Puntos - 1;
                        sesion.SaveChanges();
                        Console.WriteLine("El monstruo " + mons[rnd].Nombre + " ha perdido 1 punto de victoria, ahora tiene " + mons[rnd].Puntos);

                    }
                    monstruo.Poder.Poseido = false;
                    monstruo.Poder = null;
                    sesion.SaveChanges();
                    break;
                case 6:
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha usado el poder Monstruo con rayo reductor");
                    List<Monstruo> mons2 = new List<Monstruo>();
                    foreach (Monstruo mon in lista)
                    {
                        if (!mon.Equals(monstruo) && mon.Vivo)
                        {
                            mons2.Add(mon);
                        }
                    }
                    if (mons2.Count != 0)
                    {
                        rnd = random.Next(mons2.Count());
                        mons2[rnd].Vidas = mons2[rnd].Vidas - 1;
                        sesion.SaveChanges();
                        if (mons2[rnd].Tokio)
                        {
                            Monstruo nece2 = mons2[rnd];
                            EntrarTokio(ref monstruo, ref nece2, sesion);
                        }                        
                        ComprobarVivo(sesion, ref monstruo);
                        Console.WriteLine("El monstruo " + mons2[rnd].Nombre + " ha perdido 1 vida, ahora tiene " + mons2[rnd].Vidas);

                    }
                    monstruo.Poder.Poseido = false;
                    monstruo.Poder = null;
                    sesion.SaveChanges();
                    break;
                case 8:
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha usado el poder Mimetismo");
                    List<Monstruo> mons3 = new List<Monstruo>();
                    foreach (Monstruo mon in lista)
                    {
                        if (!mon.Equals(monstruo) && mon.Vivo)
                        {
                            mons3.Add(mon);
                        }
                    }
                    if (mons3.Count != 0)
                    {
                        rnd = random.Next(mons3.Count());
                        int interVidas;
                        int interPuntos;
                        interVidas = mons3[rnd].Vidas;
                        interPuntos = mons3[rnd].Puntos;
                        mons3[rnd].Vidas = monstruo.Vidas;
                        mons3[rnd].Puntos = monstruo.Puntos;
                        monstruo.Vidas = interVidas;
                        monstruo.Puntos = interPuntos;
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " tiene ahora " + monstruo.Vidas);
                        Console.WriteLine("El monstruo " + mons3[rnd].Nombre + " tiene ahora " + mons3[rnd].Vidas);
                        sesion.SaveChanges();
                    }
                    monstruo.Poder.Poseido = false;
                    monstruo.Poder = null;
                    sesion.SaveChanges();
                    break;
            }
        }

        //Metodos principales
        public static Monstruo Start(ref List<Monstruo> m, ref List<Jugador> j, Sesion sesion)
        {
            var rand = new Random();
            int turno = rand.Next(PLAYER);
            // Y creamos a partida.
            sesion.Partidas.Add(new Partida((turno + 1), PLAYER));
            sesion.SaveChanges();
            int o = sesion.Partidas.Count<Partida>();
            Partida part = sesion.Partidas.Find(o);
            Console.WriteLine("Se ha creado la partida con id " + part.PartidaID + " Jugaran " + PLAYER + " jugadores");
            Monstruo monstruo;
            int nJugador;
            int nMonstruo;
            List<int> jugadores = new List<int>(); //lista temporal para evitar repetir jugadores.
            List<int> monstruos = new List<int>(); //lista temporal para evitar repetir monstruos.
            Boolean repe = false;

            for (int i = 0; i < PLAYER; i++)
            {
                repe = false;
                do
                {
                    nJugador = rand.Next(8);
                    repe = jugadores.Contains(nJugador) ? true : false;
                    if (!repe) jugadores.Add(nJugador);
                } while (repe);

                sesion.Jugadores.Add(j[nJugador]);

                do
                {
                    nMonstruo = rand.Next(6);
                    repe = monstruos.Contains(nMonstruo) ? true : false;
                    if (!repe) monstruos.Add(nMonstruo);
                } while (repe);

                m[nMonstruo].Jugador = j[nJugador]; // Asignamos jugador al monstruo.
                sesion.Monstruos.Add(m[nMonstruo]); // Añadimos monstruo a la bd.

                Console.WriteLine("El Jugador " + j[nJugador].Nombre + " " + j[nJugador].Apellido + " Juega con el monstruo " + m[nMonstruo].Nombre);
            }
            Console.WriteLine("Comienza jugando el jugador " + (turno + 1) + "(" + j[jugadores[turno]].Nombre + " " + j[jugadores[turno]].Apellido + ")");
            sesion.SaveChanges();
            monstruo = sesion.Monstruos.Find(turno + 1);
            Console.WriteLine("El monstruo " + monstruo.Nombre + " ha entrado en Tokio");
            monstruo.Tokio = true;
            sesion.SaveChanges();
            return monstruo;
        }

        public static int[] Roll()
        {
            var rand = new Random();
            int[] dados = new int[6];
            for (int i=0; i<6; i++)
            {
                dados[i] = rand.Next(6) + 1;
            }
            return dados;
        }

        public static void SolveRoll(int[] dados, Sesion sesion, ref Monstruo monstruo)
        {
            int uno = 0;
            int dos = 0;
            int tres = 0;
            int trio = 0;
            for (int i = 0; i < dados.Length; i++)
            {
                switch (dados[i])
                {
                    case 1:
                        Console.WriteLine("El dado numero " + (i + 1) + " ha salido " + dados[i]);
                        uno++;
                        break;
                    case 2:
                        Console.WriteLine("El dado numero " + (i + 1) + " ha salido " + dados[i]);
                        dos++;
                        break;
                    case 3:
                        Console.WriteLine("El dado numero " + (i + 1) + " ha salido " + dados[i]);
                        tres++;
                        break;
                    case 4:
                        Console.WriteLine("El dado numero " + (i + 1) + " ha salido " + dados[i]);
                        monstruo.Energia = monstruo.Energia + 1;
                        sesion.SaveChanges();
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 1 de energia, ahora tiene " + monstruo.Energia);
                        break;
                    case 5:
                        Console.WriteLine("El dado numero " + (i + 1) + " ha salido " + dados[i]);
                        if (monstruo.Tokio)
                        {
                            var monstruos = sesion.Monstruos.ToList();
                            foreach (Monstruo mon in monstruos)
                            {
                                if (!mon.Tokio && mon.Vivo)
                                {
                                    mon.Vidas = mon.Vidas - 1;
                                    sesion.SaveChanges();
                                    Console.WriteLine("El monstruo " + mon.Nombre + " ha perdido 1 vida, ahora tiene " + mon.Vidas);
                                }
                            }
                            ComprobarVivo(sesion, ref monstruo);
                        }
                        else
                        {
                            var monstruos = sesion.Monstruos.ToList();
                            Monstruo estaentokio = null;
                            foreach (Monstruo mon in monstruos)
                            {
                                if (mon.Tokio && mon.Vivo)
                                {
                                    estaentokio = mon;
                                }
                            }
                            estaentokio.Vidas = estaentokio.Vidas - 1;
                            sesion.SaveChanges();
                            Console.WriteLine("El monstruo " + estaentokio.Nombre + " ha perdido 1 vida, ahora tiene " + estaentokio.Vidas);
                            Boolean entrar = ComprobarVivo(sesion, ref monstruo);
                            if (!entrar)
                            {
                                EntrarTokio(ref monstruo, ref estaentokio, sesion);
                            }                            
                        }
                        break;
                    case 6:
                        Console.WriteLine("El dado numero " + (i + 1) + " ha salido " + dados[i]);
                        monstruo.Vidas = monstruo.Vidas + 1;
                        sesion.SaveChanges();
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 1 vida, ahora tiene " + monstruo.Vidas);
                        break;
                }
                if (uno == 3)
                {
                    monstruo.Puntos = monstruo.Puntos + 1;
                    sesion.SaveChanges();
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 1 punto de victoria, ahora tiene " + monstruo.Puntos);
                    trio = 1;
                    uno = 0;
                }
                else if (dos == 3)
                {
                    monstruo.Puntos = monstruo.Puntos + 2;
                    sesion.SaveChanges();
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 2 punto de victoria, ahora tiene " + monstruo.Puntos);
                    trio = 2;
                    dos = 0;
                }
                else if (tres == 3)
                {
                    monstruo.Puntos = monstruo.Puntos + 3;
                    sesion.SaveChanges();
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 3 punto de victoria, ahora tiene " + monstruo.Puntos);
                    trio = 3;
                    tres = 0;
                }
                if (trio != 0)
                {
                    if (trio == 1 && uno == 1)
                    {
                        monstruo.Puntos = monstruo.Puntos + 1;
                        sesion.SaveChanges();
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 1 punto de victoria, ahora tiene " + monstruo.Puntos);
                        uno = 0;
                    }
                    else if (trio == 2 && dos == 1)
                    {
                        monstruo.Puntos = monstruo.Puntos + 1;
                        sesion.SaveChanges();
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 1 punto de victoria, ahora tiene " + monstruo.Puntos);
                        dos = 0;
                    }
                    else if (trio == 3 && tres == 1)
                    {
                        monstruo.Puntos = monstruo.Puntos + 1;
                        sesion.SaveChanges();
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha ganado 1 punto de victoria, ahora tiene " + monstruo.Puntos);
                        tres = 0;
                    }
                }



            }
        }

        private static void SolvePowerCarts(Sesion sesion, ref Monstruo monstruo)
        {
            List<Monstruo> poderes = new List<Monstruo>();
            List<Monstruo> lista = sesion.Monstruos.ToList();
            Random random = new Random();
            int rnd;
            if (monstruo.Poder != null)
            {
                Console.WriteLine("El monstruo " + monstruo.Nombre + " ya tiene un poder y no puede comprar otro");
                rnd = random.Next(2);
                if (rnd == 0)
                {
                    UsarPoder(ref monstruo, sesion, monstruo.Poder.CosteEnergia);
                }
                else
                {
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " ha decidido no usar su poder");
                }
            }
            else
            {
                Console.WriteLine("El monstruo " + monstruo.Nombre + " no tiene ningun poder que usar");
                foreach (Monstruo mon in lista)
                {
                    if (mon.CosteEnergia != 0 && mon.CosteEnergia <= monstruo.Energia && !mon.Poseido)
                    {
                        poderes.Add(mon);
                    }
                }
                if (poderes.Count() > 0)
                {
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " puede comprar " + poderes.Count() + " poderes");
                    rnd = random.Next(2);
                    if (rnd == 0)
                    {
                        Monstruo pod = poderes[random.Next(poderes.Count())];
                        monstruo.Poder = pod;
                        pod.Poseido = true;
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " ha comprado el poder " + pod.Nombre);
                        sesion.SaveChanges();
                    }
                    else
                    {
                        Console.WriteLine("El monstruo " + monstruo.Nombre + " no quiere comprar ningun poder este turno");
                    }
                }
                else
                {
                    Console.WriteLine("El monstruo " + monstruo.Nombre + " no puede comprar ningun poder");
                }
            }

        }

        private static Monstruo Reassign(Sesion sesion, ref Monstruo m, ref Boolean ganador)
        {
            //Ganadir por puntos.
            if (m.Puntos >19)
            {
                ganador = true;
                return m;
            }

            //Ganador por eliminación rivales.
            var monstruos = sesion.Monstruos.ToList();
            int muertos = 0;
            foreach (Monstruo mon in monstruos)
            {
                if (!mon.Vivo)
                {
                    muertos++;
                }
            }

            if (muertos == PLAYER+3)
            {
                ganador = true;
                return m;
            }

            // Asignación del turno.
            Boolean muerto = true;
            if (m.MonstruoID < PLAYER)
            {
                m = sesion.Monstruos.Find(m.MonstruoID + 1);
                while (muerto)
                {
                    if (!m.Vivo && m.MonstruoID < PLAYER)
                    {
                        m = sesion.Monstruos.Find(m.MonstruoID + 1);
                    }
                    else if (!m.Vivo && m.MonstruoID == PLAYER)
                    {
                        m = sesion.Monstruos.Find(1);
                    }
                    else
                    {
                        muerto = false;
                    }
                }
            }
            else
            {
                m = sesion.Monstruos.Find(1);
                while (muerto)
                {
                    if (!m.Vivo)
                    {
                        m = sesion.Monstruos.Find(m.MonstruoID + 1);
                    }
                    else
                    {
                        muerto = false;
                    }
                }
            }
            int i = sesion.Partidas.Count<Partida>();
            Partida part = sesion.Partidas.Find(i);
            part.Turno = m.MonstruoID;
            sesion.SaveChanges();
            return m;
        }
    }
}

