﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyoOriol
{
    public class Sesion: DbContext
    {

        public Sesion(): base("Juego")
        {
            Database.SetInitializer<Sesion>(new DropCreateDatabaseAlways<Sesion>());
        }

        public DbSet<Partida> Partidas { get; set; }
        public DbSet<Jugador> Jugadores { get; set; }
        public DbSet<Monstruo> Monstruos { get; set; }


    }
}
