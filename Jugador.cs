﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTokyoOriol
{
    public class Jugador
    {

        public Jugador()
        {

        }

        public Jugador(string nombre, string apellido)
        {
            Nombre = nombre;
            Apellido = apellido;
        }

        public int JugadorID { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public virtual ICollection<Monstruo> Monstruos { get; set; }


    }
}
